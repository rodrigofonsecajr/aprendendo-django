# Generated by Django 3.0.6 on 2020-10-04 16:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('imobiliariaSA', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fotos',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('foto', models.ImageField(blank=True, help_text='Escolha uma foto.', upload_to='media/fotos/%Y/%m/%d', verbose_name='Foto')),
                ('descricao', models.TextField(help_text='Descrição', verbose_name='Descrição')),
                ('imovel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imobiliariaSA.Imovel')),
            ],
            options={
                'verbose_name_plural': 'Fotos dos imóveis',
                'db_table': 'tab_fotosImoveis',
                'managed': True,
            },
        ),
    ]
