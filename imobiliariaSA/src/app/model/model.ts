
export class TipoImovel {
    public id: number;
    public nome: string;
    public status: boolean;
}

export class Categoria {
    public id: number;
    public nome: string;
    public status: boolean;
}

export class Imovel {
    public id: number;
    public tipoImovel: TipoImovel;
    public categoria: Categoria;
    public nome: string;
    public descricao: string;
    public status: boolean;
    public fotos: File[];
    // public fotos: Fotos[];
}

export class Fotos {
    public id: number;
    public imovel: number;
    public foto: any;
    public descricao: string;
}
