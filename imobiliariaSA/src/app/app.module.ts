import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './autenticador/login/login.component';
import { Constantes } from 'src/app/util/constantes';
import { HomeComponent } from './paginas/home/home.component';
import { InterceptadorRequest } from './util/interceptador-request.interceptor';

import { NgxSpinnerModule } from 'ngx-spinner';
import { CategoriaComponent } from './paginas/categoria/categoria.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Plugin
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ImovelComponent } from './paginas/imovel/imovel.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CategoriaComponent,
    ImovelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    NgbModule,
    NgxDropzoneModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    Constantes
    , [
      {provide: HTTP_INTERCEPTORS, useClass: InterceptadorRequest, multi: true}
    ]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
