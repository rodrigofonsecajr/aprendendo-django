import { Component, OnInit } from '@angular/core';
import { CategoriaService } from 'src/app/servicos/categoria.service';
import { Acao } from 'src/app/util/acao';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Plugin
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TipoImovelService } from 'src/app/servicos/tipo-imovel.service';
import { ImovelService } from 'src/app/servicos/imovel.service';

// Models
import { TipoImovel, Categoria, Imovel, Fotos } from 'src/app/model/model';

// Utilidades
import * as Utilidades from 'src/app/util/utilidades';

@Component({
  selector: 'app-imovel',
  templateUrl: './imovel.component.html',
  styleUrls: ['./imovel.component.scss']
})
export class ImovelComponent implements OnInit, Acao {

  public categorias: Categoria[];
  public tiposImoveis: TipoImovel[];
  public imoveis: Imovel[];
  public imovelForm: FormGroup;
  public loading = false;
  public submitted = false;
  public files: File[] = [];

  constructor(private categoriaService: CategoriaService
            , private tipoImovelService: TipoImovelService
            , private imovelService: ImovelService
            , private modal: NgbModal
            , private formBuilder: FormBuilder) { }

  async pesquisar() {
    this.categoriaService.pesquisar().then(async (retorno: any) => {
      this.categorias = retorno;
    });
    this.tipoImovelService.pesquisar().then(async (retorno: any) => {
      this.tiposImoveis = retorno;
    });
    this.imovelService.pesquisar().then(async (retorno: any) => {
      this.imoveis = retorno;
    });
  }

  adicionar() {
    this.imovelForm = this.formBuilder.group({
      nome: ['', Validators.required],
      descricao: ['', Validators.required],
      categoria: [this.categorias.length > 0 ? this.categorias[0].id : '', Validators.required],
      tipoImovel: [this.tiposImoveis.length > 0 ? this.tiposImoveis[0].id : '', Validators.required],
      fotos: [''],
      status: [true]
    });
  }

  async editar(id: number) {
    this.imovelService.pesquisar(id).then(async (retorno: any) => {
      this.imovelForm = this.formBuilder.group({
        id: [retorno.id],
        nome: [retorno.nome, Validators.required],
        descricao: [retorno.descricao, Validators.required],
        tipoImovel: [retorno.tipoImovel, Validators.required],
        categoria: [retorno.categoria, Validators.required],
        status: [retorno.status]
      });
    });
  }

  remover(id: number) {
    this.imovelService.remover(id).then(async () => {
      this.pesquisar();
    });
  }

  openBackDropCustomClass(content) {
    this.modal.open(content, {backdropClass: 'light-blue-backdrop'});
  }

  ngOnInit(): void {
    this.pesquisar();
  }

  get f() { return this.imovelForm.controls; }

  async onSubmit() {
    this.submitted = true;
    if (this.imovelForm.invalid) {
        return;
    }
    this.loading = true;
    const imovel = new Imovel();
    imovel.categoria = this.imovelForm.controls.categoria.value;
    imovel.tipoImovel = this.imovelForm.controls.tipoImovel.value;
    imovel.descricao = this.imovelForm.controls.descricao.value;
    imovel.nome = this.imovelForm.controls.nome.value;

    // imovel.fotos = [];
    // for await (const arquivo of this.files) {
    //   const foto = new Fotos();
    //   foto.descricao = arquivo.name;
    //   foto.foto = await Utilidades.default.conversorImagemEmBase64(arquivo);
    //   imovel.fotos.push(foto);
    // }

    imovel.fotos = this.files;

    if (this.imovelForm.controls.id) {
      this.imovelService.atualizar(imovel).then(
        (data: any) => {
          this.pesquisar();
          this.modal.dismissAll();
        }).finally(() => {this.loading = false; this.submitted = false; });
    } else {
      this.imovelService.adicionar(imovel).then(
        (data: any) => {
          this.pesquisar();
          this.modal.dismissAll();
        }).finally(() => {this.loading = false; this.submitted = false; });
    }
  }

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  async converte(file: File) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    return reader.onload = () => {
      return reader.result;
    };
  }

}
