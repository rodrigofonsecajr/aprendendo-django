import { Component, OnInit } from '@angular/core';
import { CategoriaService } from 'src/app/servicos/categoria.service';
import { Acao } from 'src/app/util/acao';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Plugin
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss']
})
export class CategoriaComponent implements OnInit, Acao {

  public categorias: [];
  public categoriaForm: FormGroup;
  public loading = false;
  public submitted = false;

  constructor(private categoriaService: CategoriaService
            , private modal: NgbModal
            , private formBuilder: FormBuilder) { }

  async pesquisar() {
    this.categoriaService.pesquisar().then(async (retorno:any) => {
      this.categorias = retorno;
    });
  }

  adicionar() {
    this.categoriaForm = this.formBuilder.group({
      nome: ['', Validators.required],
      status: [true]
    });
  }

  async editar(id: number) {
    this.categoriaService.pesquisar(id).then(async (retorno:any) => {
      this.categoriaForm = this.formBuilder.group({
        id: [retorno.id],
        nome: [retorno.nome, Validators.required],
        status: [retorno.status]
      });
    });
  }

  remover(id: number) {
    this.categoriaService.remover(id).then(async () => {
      this.pesquisar();
    })
  }

  openBackDropCustomClass(content) {
    this.modal.open(content, {backdropClass: 'light-blue-backdrop'});
  }

  ngOnInit(): void {
    this.pesquisar();
  }

  get f() { return this.categoriaForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.categoriaForm.invalid) {
        return;
    }
    this.loading = true;
    if (this.categoriaForm.controls.id) {
      this.categoriaService.atualizar(this.categoriaForm.value).then(
        (data: any) => {
          this.pesquisar();
          this.modal.dismissAll();
        }).finally(() => {this.loading = false; this.submitted = false; });
    } else {
      this.categoriaService.adicionar(this.categoriaForm.value).then(
        (data: any) => {
          this.pesquisar();
          this.modal.dismissAll();
        }).finally(() => {this.loading = false; this.submitted = false; });
    }
    
  }

}
