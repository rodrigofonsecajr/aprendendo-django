import { Injectable } from '@angular/core';
import { AuxiliarCrud } from './util/auxiliar-crud';

// Componentes
import { HttpClient } from '@angular/common/http';

// Útil
import { Constantes } from 'src/app/util/constantes';
import * as Utilidades from '../util/utilidades';

@Injectable({
  providedIn: 'root'
})
export class ImovelService extends AuxiliarCrud {

  constructor(public http: HttpClient, public constantes: Constantes) {
    super(http, constantes, 'imoveis/');
  }

  async adicionar(campos: any) {
    return await this.http.post<any[]>(this.constantes.backUrlBase + 'imoveis/'
        ,   Utilidades.default.gerarFormData(campos)).toPromise();
  }

}
