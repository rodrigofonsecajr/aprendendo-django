import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Constantes } from 'src/app/util/constantes';
import { AuxiliarCrud } from 'src/app/servicos/util/auxiliar-crud';

@Injectable({
  providedIn: 'root'
})
export class ImoveisService extends AuxiliarCrud {

  constructor(public http: HttpClient, public constantes: Constantes) {
    super(http, constantes, 'imoveis/');
  }
}
