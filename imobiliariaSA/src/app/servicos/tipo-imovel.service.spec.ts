import { TestBed } from '@angular/core/testing';

import { TipoImovelService } from './tipo-imovel.service';

describe('TipoImovelService', () => {
  let service: TipoImovelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoImovelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
