export interface Acao {
    pesquisar();
    adicionar();
    editar(id: number);
    remover(id: number);
}
