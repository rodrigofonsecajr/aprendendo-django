import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

// Plugin
import { NgxSpinnerService } from 'ngx-spinner';
import { tap, catchError } from 'rxjs/operators';

// Serviços
import { AutenticacaoService } from 'src/app/servicos/autenticacao.service';

@Injectable()
export class InterceptadorRequest implements HttpInterceptor {

  constructor(private autenticacaoService: AutenticacaoService
            , private loading: NgxSpinnerService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.loading.show();
    const usuario = this.autenticacaoService.currentUserValue;
    if (usuario && usuario.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Token ${usuario.token}`
        }
      });
    }
    return next.handle(request).pipe(tap((ev: HttpEvent<any>) => {
      if (ev instanceof HttpResponse) {
          this.loading.hide();
      }
    }));
  }
}
