import { TestBed } from '@angular/core/testing';

import { InterceptadorRequestInterceptor } from './interceptador-request.interceptor';

describe('InterceptadorRequestInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      InterceptadorRequestInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: InterceptadorRequestInterceptor = TestBed.inject(InterceptadorRequestInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
